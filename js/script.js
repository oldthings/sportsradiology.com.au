$(document).ready(function(){
	

	if ( $("#showFAQ").length > 0 ) {

		if ( window.location.hash != "" ) {
			$("#showFAQ").load( "faqContent.html #" + window.location.hash );

			$(".faqList a[href='"+window.location.hash+"']").addClass("selected");
		}

		$(".faqList a").click(function(e){

			$(".faqList a").removeClass("selected");
			$(this).addClass("selected");

			$("#showFAQ").load( "/faq/content " + $(this).attr("href") );

		});

	};
	

	
	if ( $("#conditions").length > 0 ) {

		$("#conditions a.condition").each(function(i){			
			var conditionLink = $(this);
			var conditionList = $( "#" + conditionLink.attr("rel") );

			if ( conditionList.length > 0 ) {

				conditionLink.bind( "click", function(e){
					e.preventDefault();

					$("#conditions a.condition").removeClass("selected");
					$(".conditionList").removeClass("showList");

					conditionLink.addClass("selected");
					conditionList.addClass("showList");
					$("#helpButton").css( {"opacity":"1"} );
				});

			};

		});

		$(".conditionList .title").live( "click", function(e){

			e.preventDefault();

			var conditionDetails = $(this).parent();
			var content = conditionDetails.find( ".content" );

			if ( !conditionDetails.hasClass("selected") ) {
				conditionDetails.addClass("selected");
				content.css({"height":content[0].scrollHeight + "px" });
			} else {
				conditionDetails.removeClass("selected");
				content.css({"height":"0px" });
			}



		});

	};

});